#include "raytracer.h"
#include "ui_raytracer.h"
#include "sphere.h"
#include "triangle.h"
#include "plane.h"
#include "hit.h"
#include "scenes/sun.h"

#include "QColorDialog"
#include "QFileDialog"
#include "QMessageBox"
#include "QTextStream"
#include "QTime"

#include "math.h"
#include "limits"

RayTracer::RayTracer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RayTracer)
{
    // The initial color of surfaces
    // Teddy bear brown
    surfaceColor = QColor(139, 99, 55);

    // Red
    //surfaceColor = QColor(255, 0, 0);

    ui->setupUi(this);
}

RayTracer::~RayTracer()
{
    delete ui;
}

QImage RayTracer::trace()
{
    // Image to be drawn
    QImage image = QImage(resolution.x(), resolution.y(), QImage::Format_ARGB32);

    // The vector horizontal to the camera
    QVector3D cameraHorizontal = QVector3D::normal(cameraDirection, cameraUp);

    // Image plane definition
    float left = -imagePlane.x()/2;
    float bottom = -imagePlane.y()/2;

    // Generate rays for each pixel (i, j)
    // Vector describing the distance of the image plane from the camera
    QVector3D w = -focalLength * cameraDirection;
    for (int j = 0; j < resolution.y(); j++)
    {
        for (int i = 0; i < resolution.x(); i++)
        {
            // The color of this pixel to return
            QColor pixelColor;

            // Determine if anti-alaising is required
            if (antiAlaisingLevel > 1)
            {
                uint red = 0;
                uint green = 0;
                uint blue = 0;

                // Number of samples to take on one side of the pixel for anti-alaising
                int aa = pow(antiAlaisingLevel, 0.5);

                // Current random number being used for anti-alaising
                uint rand = 0;

                // Take multiple semi-randomly spaced samples for the pixel and average them
                for (int p = 0; p < aa; p++)
                {
                    for (int q = 0; q < aa; q++)
                    {
                        // Random spacers for the sample
                        float verticalSpacer = (antiAlaisingRandomValues[rand++] + q) / aa;
                        float horizontalSpacer = (antiAlaisingRandomValues[rand++] + p) / aa;

                        // Vector describing the vertical component of the pixel location
                        QVector3D v = (bottom + imagePlane.y() * (j + verticalSpacer) / resolution.y()) * cameraUp;

                        // Vector describing the horizontal component of the pixel location
                        QVector3D u = (left + imagePlane.x() * (i + horizontalSpacer) / resolution.x()) * cameraHorizontal;

                        // Find the ray direction
                        QVector3D rayDirection = w + u + v;
                        QColor color = getRayColor(Ray(cameraPostion, rayDirection.normalized()), 0);
                        red += color.red();
                        green += color.green();
                        blue += color.blue();
                    }
                }
                // Average the samples
                red /= antiAlaisingLevel;
                green /= antiAlaisingLevel;
                blue /= antiAlaisingLevel;
                uint max = 255;

                // Set the color
                pixelColor = QColor(std::min(max, red), std::min(max, green), std::min(max, blue));
            }
            else
            {
                // Vector describing the vertical component of the pixel location
                QVector3D v = (bottom + imagePlane.y() * (j + 0.5) / resolution.y()) * cameraUp;

                // Vector describing the horizontal component of the pixel location
                QVector3D u = (left + imagePlane.x() * (i + 0.5) / resolution.x()) * cameraHorizontal;

                // Find the ray direction
                QVector3D rayDirection = w + u + v;
                pixelColor = getRayColor(Ray(cameraPostion, rayDirection.normalized()), 0);
            }

            // Set the pixel color
            image.setPixel(i, resolution.y() - j - 1, pixelColor.rgb());
        }
    }

    return image;
}

QColor RayTracer::getRayColor(Ray ray, int depth)
{
    QColor backgroundColor = QColor(0, 0, 0);

    // Find the closest surface to the camera
    Hit nearestHit = boundingBox.hit(ray);

     // No surfaces are hit by this ray, return the background color
    if (!nearestHit.hit)
    {
        return backgroundColor.rgb();
    }

    // Determine what light sources are visible to the surface at the point
    std::vector <QVector3D> visibleLights;
    // TODO fix this magic number
    QVector3D hoveringSurfacePoint = nearestHit.hitPoint + nearestHit.normal * 0.001;
    if (ui->shadowsCheckBox->checkState() == Qt::Checked)
    {
        for (uint i = 0; i < lightPostions.size(); i++)
        {
            // Randomize the light across a globe centered on the light
            float lightSize = ui->lightRadius->text().toFloat();
            float xRand = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/lightSize));
            float yRand = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/lightSize));
            float zRand = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/lightSize));
            if (xRand > lightSize / 2)
                xRand = - (xRand / 2);
            if (yRand > lightSize / 2)
                yRand = - (yRand / 2);
            if (zRand > lightSize / 2)
                zRand = - (zRand / 2);
            QVector3D random = QVector3D(xRand, yRand, zRand);
            QVector3D light = lightPostions[i] + random;
            QVector3D lightDirection = (light - hoveringSurfacePoint);
            Ray shadowRay = Ray(hoveringSurfacePoint, lightDirection.normalized());
            Hit shadowHit = boundingBox.hit(shadowRay);

            // If there is no collision between the light and a surface then there is no shadow
            if (!shadowHit.hit || shadowHit.distance > lightDirection.length())
            {
                visibleLights.push_back(lightPostions[i]);
            }
        }
    }
    else
    {
        // All lights are visible
        visibleLights = lightPostions;
    }

    // Determine shading of the surface at the point that ray hits it
    float aRed = ambientLightLevel *  nearestHit.surface->ambientColor.red();
    float aBlue = ambientLightLevel * nearestHit.surface->ambientColor.blue();
    float aGreen = ambientLightLevel * nearestHit.surface->ambientColor.green();
    QColor surfaceColor = QColor(aRed, aGreen, aBlue);

    // Get the mirroring component
    if (nearestHit.surface->spectralColor.rgb() > 0 && depth < ui->maxGetRayColor->text().toInt())
    {
        QVector3D reflectionDirection = ray.direction - 2*(QVector3D::dotProduct(ray.direction, nearestHit.normal))*nearestHit.normal;
        QColor reflectionColor = getRayColor(Ray(hoveringSurfacePoint, reflectionDirection.normalized()), depth + 1);
        surfaceColor.setRed(std::min((float)255, (reflectionColor.red()*((float)nearestHit.surface->spectralColor.red()/255)) + surfaceColor.red()));
        surfaceColor.setGreen(std::min((float)255, (reflectionColor.green()*((float)nearestHit.surface->spectralColor.green()/255)) + surfaceColor.green()));
        surfaceColor.setBlue(std::min((float)255, (reflectionColor.blue()*((float)nearestHit.surface->spectralColor.blue()/255)) + surfaceColor.blue()));
    }


    for (uint i = 0; i < visibleLights.size(); i++)
    {
        // Get the angle between the light normal and the surface normal
        QVector3D lightNormal = (visibleLights[i] - nearestHit.hitPoint).normalized();
        float angle = std::max((qreal) 0, QVector3D::dotProduct(nearestHit.normal, lightNormal));

        // Set the diffuse component
        float diffuseLightPower = diffuseLightLevel * angle;

        // Get the level of 'mirroring' happening at this point
        QVector3D mirrorNormal = (lightNormal + -ray.direction).normalized();
        float mirrorAngle = std::max((qreal) 0, QVector3D::dotProduct(nearestHit.normal, mirrorNormal));

        // Set the spectral component
        float spectralLightPower = spectralLightLevel * pow(mirrorAngle, nearestHit.surface->phongExponent);

        float dRed = diffuseLightPower * nearestHit.surface->diffuseColor.red();
        float dBlue = diffuseLightPower * nearestHit.surface->diffuseColor.blue();
        float dGreen = diffuseLightPower * nearestHit.surface->diffuseColor.green();

        float sRed = spectralLightPower * nearestHit.surface->spectralColor.red();
        float sBlue = spectralLightPower *  nearestHit.surface->spectralColor.blue();
        float sGreen = spectralLightPower *  nearestHit.surface->spectralColor.green();

        surfaceColor.setRed(std::min((float) 255, dRed + sRed + surfaceColor.red()));
        surfaceColor.setGreen(std::min((float) 255, dGreen + sGreen + surfaceColor.green()));
        surfaceColor.setBlue(std::min((float) 255, dBlue + sBlue + surfaceColor.blue()));
    }

    return surfaceColor.rgb();
}

void RayTracer::on_traceButton_clicked()
{
    // TODO pause program until this returns

    // NOTE: edit within this statement to create a custom image!
    // Render the sphere if there is no obj file loaded
    if (ui->traceObjFileCheckBox->checkState() != Qt::Checked || ui->objFileName->text() == "")
    {
        // Drop the old surfaces
        clearSurfaces();

        // Render a fixed sphere
        QVector3D sphereCenter = QVector3D(0, 1, 0);

        Surface* sphere = new Sphere(sphereCenter,
                                     1,
                                    QColor(255,255,255),
                                     1000);
        // Semi-reflective
        sphere->spectralColor = QColor(100, 100, 100);
        surfaces.push_back(sphere);

        // Render a sphere
        sphereCenter = QVector3D(ui->sphereX->text().toFloat(),
                                           ui->sphereY->text().toFloat(),
                                           ui->sphereZ->text().toFloat());
        sphere = new Sphere(sphereCenter,
                                     ui->sphereRadius->text().toFloat(),
                                    surfaceColor,
                                     ui->spherePhong->text().toFloat());
        sphere->spectralColor = QColor(255 , 255, 255);
        surfaces.push_back(sphere);

        // Render a plane
        Plane* plane = new Plane(QVector3D(0, 0, 0), QVector3D(0, 1, 0), 100, QColor(50, 50, 50));

        // Make the plane reflective
        plane->spectralColor = QColor(255, 255, 255);
        surfaces.push_back(plane);
    }
    // Render the obj file
    else
    {
        // Parse the obj file
        try
        {
            parseObjFile(ui->objFileName->text());
        }
        catch (QString errorString)
        {
            QMessageBox::information(0, "error", errorString);
            return;
        }
    }

    // Destroy the old lights
    lightPostions.clear();

    // Add lights
    // The postions of the lights
    for (int i = 0; i < ui->lights->count(); i++)
    {
        QString light = ui->lights->item(i)->text();
        QStringList coords = light.split(' ');

        lightPostions.push_back(QVector3D(coords[0].toFloat(),
                                        coords[1].toFloat(),
                                        coords[2].toFloat()));
    }

    // The resolution of the image
    resolution = QVector2D(ui->resolutionX->text().toFloat(),
                           ui->resolutionY->text().toFloat());

    // The antiAlaising level to use
    antiAlaisingLevel = ui->antiAliasingLevel->currentText().toInt();
    if (antiAlaisingLevel > 1)
    {
        // Drop the old random values
        antiAlaisingRandomValues.clear();

        int numSamples = (resolution.x() + resolution.y()) * antiAlaisingLevel;
        for (int i = 0; i < numSamples; i++)
        {
            antiAlaisingRandomValues.push_back((float) (rand() % 100) / 100);
        }
    }

    // The size of the image plane
    imagePlane = QVector2D(ui->imagePlaneX->text().toFloat(),
                           ui->imagePlaneY->text().toFloat());

    // The position of the camera
    cameraPostion = QVector3D(ui->cameraX->text().toFloat(),
                              ui->cameraY->text().toFloat(),
                              ui->cameraZ->text().toFloat());

    // The orientation of the camera
    cameraUp = QVector3D(ui->cameraUpX->text().toFloat(),
                         ui->cameraUpY->text().toFloat(),
                         ui->cameraUpZ->text().toFloat()).normalized();

    // The opposite to the direction the camera is facing
    cameraDirection = -1 * QVector3D(ui->cameraDirectionX->text().toFloat(),
                                ui->cameraDirectionY->text().toFloat(),
                                ui->cameraDirectionZ->text().toFloat()).normalized();

    // The distance from the camera to the image
    focalLength = ui->focalLength->text().toFloat();

    diffuseLightLevel = (float) ui->diffuseLightLevel->value() / 100;
    ambientLightLevel = (float) ui->ambientLightLevel->value() / 100;
    spectralLightLevel = (float) ui->spectralLightLevel->value() / 100;

    // Initialize the bounding box
    boundingBox = BoundingBox::getBoundingBox(surfaces);
    boundingBox.bind(surfaces, 0);

    // Start a timer for the tracing
    QTime t;
    t.start();
    QImage image = trace();

    // Set the time elapsed
    float secondsElapsed = (float) t.elapsed() / 1000;
    ui->timer->setText(QString::number(secondsElapsed));

    // Display the image
    ui->imageLabel->setPixmap(QPixmap::fromImage(image));

    // Save the image
    if (ui->saveToFileName->text() != "")
        saveImage(QPixmap::fromImage(image), ui->saveToFileName->text());
}

void RayTracer::parseObjFile(QString objFileName)
{
    // Clean up
    clearSurfaces();
    objFileVectors.clear();

    // Obj files are not zero based
    objFileVectors.push_back(QVector3D());

    // Open up the file
    QFile file(objFileName);
    if(!file.open(QIODevice::ReadOnly))
    {
        throw file.errorString();
    }

    QTextStream in(&file);

    // Stream through the file line by line
    while(!in.atEnd())
    {
        QString line = in.readLine();

        // Ignore blank lines
        if (line == "")
            continue;

        QStringList fields = line.split(" ");

        // Type is the first element of the line 'v' or 'f'
        QString type = fields[0];

        // Disregard comments
        if (type.contains('#'))
            continue;

        // Malformed line
        if (type.compare("v") != 0 && type.compare("f") != 0 && type.compare("s") != 0)
        {
            throw QString("Malformed line: " + line);
        }
        // Add a vector to the list
        else if (type.compare("v") == 0 && (fields.size() == 4))
        {
            float p1 = fields[1].toFloat();
            float p2 = fields[2].toFloat();
            float p3 = fields[3].toFloat();
            objFileVectors.push_back(QVector3D(p1, p2, p3));
        }
        // Add a triangle to the list
        else if (type.compare("f") == 0 && (fields.size() == 4))
        {
            QVector3D* v1 = &objFileVectors[fields[1].toInt()];
            QVector3D* v2 = &objFileVectors[fields[2].toInt()];
            QVector3D* v3 = &objFileVectors[fields[3].toInt()];
            Surface* triangle = new Triangle(v1, v2, v3, surfaceColor);

            surfaces.push_back(triangle);
        }
        // Add a sphere to the list
        else if (type.compare("s") == 0 && (fields.size() == 3))
        {
            QVector3D v1 = objFileVectors[fields[1].toInt()];
            Surface * sphere = new Sphere(v1, fields[2].toFloat(), surfaceColor);

            surfaces.push_back(sphere);
        }
    }

    file.close();
}

void RayTracer::clearSurfaces()
{
    // Destroy the old surfaces
    for (int i = surfaces.size() - 1; i >= 0; i--)
    {
        delete surfaces[i];
        surfaces.pop_back();
    }
}

void RayTracer::clearScenes()
{
    // Destroy the old scenes
    for (int i = scenes.size() - 1; i >= 0; i--)
    {
        delete scenes[i];
        scenes.pop_back();
    }
}

void RayTracer::saveImage(QPixmap image, QString fileName)
{
    if (fileName != "")
    {
        QFile file(fileName);
        if(!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(0, "error", file.errorString());
            return;
        }
        image.save(&file, "PNG");
        file.close();
    }
}

void RayTracer::on_setColorButton_clicked()
{
    surfaceColor = QColorDialog::getColor(surfaceColor, this);
}

void RayTracer::on_addLightButton_clicked()
{
    QString coordinates = ui->lightX->text() + " " +
             ui->lightY->text() + " " +
             ui->lightZ->text();
    ui->lights->addItem(coordinates);;
}

void RayTracer::on_deleteLight_clicked()
{
    QList <QListWidgetItem *> selectedLights = ui->lights->selectedItems();
    for (int i = 0; i < selectedLights.size(); i++)
    {
        ui->lights->removeItemWidget(selectedLights[i]);
        delete selectedLights[i];
    }
}

void RayTracer::on_addObjFileButton_clicked()
{
    QString objFileName = QFileDialog::getOpenFileName(this,
        tr("Open .obj file"), QDir::homePath(), tr("Object Files (*.obj)"));
    ui->objFileName->setText(objFileName);
}

void RayTracer::on_saveToFileButton_clicked()
{
    QString saveToFileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                QDir::homePath(),
                                tr("Images (*.png)"));
    ui->saveToFileName->setText(saveToFileName);

    // Save the image if available
    if(ui->imageLabel->pixmap())
        saveImage(*(ui->imageLabel->pixmap()), saveToFileName);
}
