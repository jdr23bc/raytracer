#ifndef RAYTRACER_H
#define RAYTRACER_H

#include "QMainWindow"
#include "QVector2D"
#include "QVector3D"
#include "surface.h"
#include "scenes/scene.h"
#include "boundingbox.h"

namespace Ui {
class RayTracer;
}

class RayTracer : public QMainWindow
{
    Q_OBJECT

public:
    explicit RayTracer(QWidget *parent = 0);
    ~RayTracer();

private slots:
    // Start tracing
    void on_traceButton_clicked();

    // Set the color of the orb or the .obj file surfaces
    void on_setColorButton_clicked();

    // Add a light to the scene
    void on_addLightButton_clicked();

    // Remove a light from the scene
    void on_deleteLight_clicked();

    // Specify a path to a .obj file
    void on_addObjFileButton_clicked();

    // Specify a path to save a file
    void on_saveToFileButton_clicked();

private:
    Ui::RayTracer *ui;

    // The resolution of the image
    QVector2D resolution;

    // The size of the image plane
    QVector2D imagePlane;

    // The position of the camera
    QVector3D cameraPostion;

    // The orientation of the camera
    QVector3D cameraUp;

    // The direction the camera is facing
    QVector3D cameraDirection;

    // The distance of the camera from the image plane
    float focalLength;

    // The number of samples to be taken for a pixel
    int antiAlaisingLevel;

    // The random values to be used for antialaising
    std::vector<float> antiAlaisingRandomValues;

    // The diffuse light intensity
    float diffuseLightLevel;

    // The spectral light intensity;
    float spectralLightLevel;

    // The ambient light intensity
    float ambientLightLevel;

    // The postions of the lights
    std::vector <QVector3D> lightPostions;

    // The bounding box for the surfaces in the scene
    BoundingBox boundingBox;

    // The surfaces in the scene
    std::vector<Surface*> surfaces;

    // The scenes in the scene
    std::vector<Scene*> scenes;

    // The vectors of a .obj file
    std::vector<QVector3D> objFileVectors;

    // The color of the surfaces in the scene
    QColor surfaceColor;

    /**
     * Generate the rays and trace the scene
     * @return The resulting image
     */
    QImage trace();

    /**
     * Return the color of the given ray
     *
     * @param ray The ray to find a color for
     * @param depth The number of times this has been called
     * @return the color of the given ray
     */
    QColor getRayColor(Ray ray, int depth);

    /**
     * Populate surfaces and objectFileVectors with information parsed from the .obj file.
     * If the obj file is invalid throw a string describing the problem.
     * @param objFileName
     */
    void parseObjFile(QString objFileName);

    /**
     * @return The bounding box for the surfaces
     */
    BoundingBox getBoundingBox();

    /**
     * Destroy all objects in the surfaces list
     */
    void clearSurfaces();

    /**
     * Destroy all objects in the scenes list
     */
    void clearScenes();

    /**
      * Saves an image to a file
      */
    void saveImage(QPixmap image, QString fileName);
};

#endif // RAYTRACER_H
