#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include "surface.h"

class BoundingBox: public Surface
{
public:
    BoundingBox();
    BoundingBox(QVector3D min, QVector3D max);
    ~BoundingBox();

    /**
     * @param ray
     * @return false if no surface in the box is hit, true otherwise
     */
    Hit hit(Ray ray);

    // See description in surface.h
    BoundingBox getBoundingBox();

    /**
     * @param vectors
     * @return The minimum bounding box for the given list of vectors
     */
    static BoundingBox getBoundingBox(std::vector<QVector3D> vectors);

    /**
     * @param surfaces
     * @return The minimum bounding box for the given list of surfaces
     */
    static BoundingBox getBoundingBox(std::vector<Surface*> surfaces);

    /**
     * Bind the given surfaces, all assumed to be within the box. If more
     * than DIVIDE_ON surfaces are in the given list, then create two sub
     * bounding boxes, by spliting along the given axis. If any surfaces lie on
     * the splitting plane then create a bounding box for those surfaces.
     * @param surfaces The surfaces to bind
     * @param axis The axis to divide the box on 0=X, 1=Y, 2=Z
     */
    void bind(std::vector<Surface*> surfaces, int axis);

    /**
     * @return true if the surface is contained by the box
     */
    bool contains(Surface* surface);

    // The tipping point for creating sub-bounding boxes
    static const int DIVIDE_ON = 2;

    // The bounds of this box, 0=min, 1=max
    QVector3D bounds [2];

    // The surfaces contained by this box
    std::vector<Surface*> children;
};

#endif // BOUNDINGBOX_H
