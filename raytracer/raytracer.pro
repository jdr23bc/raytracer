#-------------------------------------------------
#
# Project created by QtCreator 2014-11-26T14:24:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = raytracer
TEMPLATE = app


SOURCES += main.cpp\
        raytracer.cpp \
    sphere.cpp \
    triangle.cpp \
    boundingbox.cpp \
    ray.cpp \
    hit.cpp \
    plane.cpp \
    scenes/sun.cpp \
    scenes/scene.cpp

HEADERS  += raytracer.h \
    surface.h \
    sphere.h \
    triangle.h \
    boundingbox.h \
    ray.h \
    hit.h \
    scenes/sun.h \
    scenes/scene.h \
    plane.h

QMAKE_CXXFLAGS += -std=c++11

FORMS    += raytracer.ui
