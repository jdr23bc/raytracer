#include "boundingbox.h"

BoundingBox::BoundingBox()
{
    bounds[0] = QVector3D(0,0,0);
    bounds[1] = bounds[0];
}

BoundingBox::BoundingBox(QVector3D min, QVector3D max)
{
    bounds[0] = min;
    bounds[1] = max;
}

BoundingBox::~BoundingBox()
{
}

Hit BoundingBox::hit(Ray ray)
{
    float min, max, yMin, yMax, zMin, zMax;

    min = (bounds[ray.signX].x() - ray.origin.x()) * ray.invDirection.x();
    max = (bounds[1 - ray.signX].x() - ray.origin.x()) * ray.invDirection.x();
    yMin = (bounds[ray.signY].y() - ray.origin.y()) * ray.invDirection.y();
    yMax = (bounds[1 - ray.signY].y() - ray.origin.y()) * ray.invDirection.y();

    if ((min > yMax) || (yMin > max))
        return Hit(false);
    if (yMin > max)
        min = yMin;
    if (yMax < max)
        max = yMax;

    zMin = (bounds[ray.signZ].z() - ray.origin.z()) * ray.invDirection.z();
    zMax = (bounds[1 - ray.signZ].z() - ray.origin.z()) * ray.invDirection.z();

    if ((min > zMax) || (max < zMin))
        return Hit(false);

    // The box is hit, check if any surfaces within the box are hit, if so return the closest
    Hit nearestHit = Hit(false);
    for(uint i = 0; i < children.size(); i++)
    {
        Hit hit = children[i]->hit(ray);
        // Check if the surface was hit
        if (hit.hit)
        {
            // If no surface has been hit yet, this is the closest surface to the ray
            if (!nearestHit.hit)
            {
                nearestHit = hit;
            }
            // If this intersection is closer then replace
            else if (hit.distance < nearestHit.distance)
            {
                nearestHit = hit;
            }
        }
    }
    return nearestHit;
}

BoundingBox BoundingBox::getBoundingBox()
{
    return *this;
}

void BoundingBox::bind(std::vector<Surface*> surfaces, int axis)
{
    // No need to subdivide
    if (surfaces.size() < DIVIDE_ON)
    {
        children = surfaces;
        return;
    }

    // Create two new bounding boxes for subdivision
    QVector3D max = bounds[1];
    QVector3D min = bounds[0];
    if (axis == 0)
    {
        float splitPlane = (max.x() + min.x()) / 2;
        max.setX(splitPlane);
        min.setX(splitPlane);
    } else if (axis == 1)
    {
        float splitPlane = (max.y() + min.y()) / 2;
        max.setY(splitPlane);
        min.setY(splitPlane);
    } else
    {
        float splitPlane = (max.z() + min.z()) / 2;
        max.setZ(splitPlane);
        min.setZ(splitPlane);
    }
    BoundingBox* box1 = new BoundingBox(bounds[0], max);
    BoundingBox* box2 = new BoundingBox(min, bounds[1]);

    // Get three lists of vectors: in box1, in box2, or intersecting the splitting plane
    std::vector<Surface*> box1Surfaces;
    std::vector<Surface*> box2Surfaces;
    std::vector<Surface*> planeSurfaces;
    for(uint i = 0; i < surfaces.size(); i++)
    {
        if (box1->contains(surfaces[i]))
        {
            box1Surfaces.push_back(surfaces[i]);
        }
        else if(box2->contains(surfaces[i]))
        {
            box2Surfaces.push_back(surfaces[i]);
        }
        else
        {
            planeSurfaces.push_back(surfaces[i]);
        }
    }

    // Add the 3 subboxes to the list of children
    // If there is only one surface in this box, it is not worth using
    if (box1Surfaces.size() == 1)
    {
        children.push_back(box1Surfaces[0]);
    }
    // Bind the surfaces to box 1
    else if (box1Surfaces.size() > 1)
    {
        box1->bind(box1Surfaces, (axis + 1) % 3);
        children.push_back(box1);
    }

    // If there is only one surface in this box, it is not worth using
    if (box2Surfaces.size() == 1)
    {
        children.push_back(box2Surfaces[0]);
    }
    // Bind the surfaces to box 2
    else if (box2Surfaces.size() > 1)
    {
        box2->bind(box2Surfaces, (axis + 1) % 3);
        children.push_back(box2);
    }

    // If there is only one surface in this box, it is not worth using
    if (planeSurfaces.size() == 1)
    {
        children.push_back(planeSurfaces[0]);
    }
    // Create a plane surfaces box and bind the plane surfaces to it
    else if (planeSurfaces.size() != 0)
    {
        // TODO: clean this mess up
        QVector3D* boxBounds = BoundingBox::getBoundingBox(planeSurfaces).bounds;
        BoundingBox* planeSurfaceBox = new BoundingBox(boxBounds[0], boxBounds[1]);

        if (box1Surfaces.size() > 0 && box2Surfaces.size() > 0)
        {
            planeSurfaceBox->bind(planeSurfaces, (axis + 1) % 3);
        }
        else
        {
            // Don't bother subdividing anymore if splitting didn't work once
            planeSurfaceBox->children = planeSurfaces;
        }
        children.push_back(planeSurfaceBox);
    }
}

bool BoundingBox::contains(Surface* surface)
{
    BoundingBox surfaceBox = surface->getBoundingBox();
    // Check if the surface's bounding box's minimum is greater than the box's minimum
    bool min = (surfaceBox.bounds[0].x() >= bounds[0].x()) &&
            (surfaceBox.bounds[0].y() >= bounds[0].y()) &&
            (surfaceBox.bounds[0].z() >= bounds[0].z());
    // Check if the surface's bounding box's maximum is less than the box's maximum
    bool max = (surfaceBox.bounds[1].x() <= bounds[1].x()) &&
            (surfaceBox.bounds[1].y() <= bounds[1].y()) &&
            (surfaceBox.bounds[1].z() <= bounds[1].z());
    // If both statements are true the surface is within the bounding box
    return min && max;
}

BoundingBox BoundingBox::getBoundingBox(std::vector<QVector3D> vectors)
{
    float maxX = -std::numeric_limits<float>::infinity();
    float maxY = -std::numeric_limits<float>::infinity();
    float maxZ = -std::numeric_limits<float>::infinity();
    float minX = std::numeric_limits<float>::infinity();
    float minY = std::numeric_limits<float>::infinity();
    float minZ = std::numeric_limits<float>::infinity();
    for (uint i = 0; i < vectors.size(); i++)
    {
        maxX = std::max(maxX, (float) vectors[i].x());
        maxY = std::max(maxY, (float) vectors[i].y());
        maxZ = std::max(maxZ, (float) vectors[i].z());
        minX = std::min(minX, (float) vectors[i].x());
        minY = std::min(minY, (float) vectors[i].y());
        minZ = std::min(minZ, (float) vectors[i].z());
    }
    BoundingBox box = BoundingBox(QVector3D(minX, minY, minZ), QVector3D(maxX, maxY, maxZ));
    return box;
}

BoundingBox BoundingBox::getBoundingBox(std::vector<Surface*> surfaces)
{
    float maxX = -std::numeric_limits<float>::infinity();
    float maxY = -std::numeric_limits<float>::infinity();
    float maxZ = -std::numeric_limits<float>::infinity();
    float minX = std::numeric_limits<float>::infinity();
    float minY = std::numeric_limits<float>::infinity();
    float minZ = std::numeric_limits<float>::infinity();
    for (uint i = 0; i < surfaces.size(); i++)
    {
        BoundingBox box = surfaces[i]->getBoundingBox();
        maxX = std::max(maxX, (float) box.bounds[1].x());
        maxY = std::max(maxY, (float) box.bounds[1].y());
        maxZ = std::max(maxZ, (float) box.bounds[1].z());
        minX = std::min(minX, (float) box.bounds[0].x());
        minY = std::min(minY, (float) box.bounds[0].y());
        minZ = std::min(minZ, (float) box.bounds[0].z());
    }
    BoundingBox box = BoundingBox(QVector3D(minX, minY, minZ), QVector3D(maxX, maxY, maxZ));
    return box;
}
