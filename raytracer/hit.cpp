#include "hit.h"

Hit::Hit()
{
}

Hit::Hit(bool hit)
{
    this->hit = hit;
}

Hit::Hit(QVector3D hitPoint, float distance, Surface* surface, QVector3D normal)
{
    this->hit = true;
    this->hitPoint = hitPoint;
    this->distance = distance;
    this->surface = surface;
    this->normal = normal;
}
