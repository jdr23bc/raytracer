#ifndef PLANE_H
#define PLANE_H

#include "surface.h"

class Plane: public Surface
{
public:
    Plane();
    Plane(QVector3D point, QVector3D normal, float size, QColor color);
    ~Plane();

    // See description in surface.h
    Hit hit(Ray ray);

    // See description in surface.h
    BoundingBox getBoundingBox();

    // A point on the plane
    QVector3D point;

    // The plane normal
    QVector3D normal;

    // The size defining the plane's bounding box
    float size;
};

#endif // PLANE_H
