#include "plane.h"
#include "boundingbox.h"
#include "math.h"

Plane::Plane()
{
}

Plane::Plane(QVector3D point, QVector3D normal, float size, QColor color)
{
    this->point = point;
    this->normal = normal;
    this->size = size;
    this->diffuseColor = color;
    this->spectralColor = color;
    this->ambientColor = color;
    this->phongExponent = 1;
}

Plane::~Plane()
{

}

Hit Plane::hit(Ray ray)
{
    float d1 = QVector3D::dotProduct(ray.direction, normal);
    // The line is either parallel or inside the plane, either way don't want to deal with it
    if (d1 == 0)
        return Hit(false);

    // Get the distance from ray origin to the plane
    float d2 = QVector3D::dotProduct((point - ray.origin), normal);
    float dist = d2/d1;
    if (dist < 0)
        return Hit(false);

    QVector3D hitPoint = dist * ray.direction + ray.origin;
    return Hit(hitPoint, dist, this, normal);
}

BoundingBox Plane::getBoundingBox()
{
    QVector3D vSize = QVector3D(size, size, size);
    return BoundingBox(point - vSize, point + vSize);
}
