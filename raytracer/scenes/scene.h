#ifndef SCENE_H
#define SCENE_H

#include "surface.h"

// A class containing surfaces that comprise a scene
class Scene
{
public:
    Scene();
    virtual ~Scene(){}

    // Surfaces that comprise the scene
    std::vector<Surface *> surfaces;
};

#endif // SCENE_H
