#include "sun.h"
#include "triangle.h"
#include "sphere.h"
#include "math.h"

Sun::Sun()
{
}

Sun::Sun(QColor sunColor, QColor ray1Color, QColor ray2Color, int rayAngle, float rayLength)
{
    // Rays can't have an angle greater than 90 degrees
    if (rayAngle > 90)
    {
        rayAngle = 90;
    }

    // Round up to the nearest factor of 90
    rayAngle += (90 % rayAngle) / ceil((90 / rayAngle));

    // Convert ray angle to radians
    float rads = rayAngle * PI / 180;

    // Create the orb of the sun
    sunOrb = new Sphere(QVector3D(0, 0, 0), 1, sunColor, 1);
    // The sun is not reflective
    sunOrb->spectralColor = QColor(0, 0, 0);
    surfaces.push_back(sunOrb);

    // Create the rays of the sun
    QVector3D* origin = new QVector3D(0, 0, 0);
    QVector3D* end1 = new QVector3D(rayLength, 0, 0);
    QVector3D* end2;
    trianglePoints.push_back(origin);
    trianglePoints.push_back(end1);
    float currentRads = 0;
    for (int i = 0; i < 360 / rayAngle; i++)
    {
        // Determine second end point of the triangle
        currentRads += rads;
        end2 = new QVector3D(rayLength * cos(currentRads),
                             rayLength * sin(currentRads),
                             0);
        trianglePoints.push_back(end2);

        // Switch colors
        QColor triangleColor;
        if (i % 2 == 0)
            triangleColor = ray1Color;
        else
            triangleColor = ray2Color;

        // Create a new triangle
        Triangle* triangle = new Triangle(origin, end1, end2, triangleColor, 0.5);
        surfaces.push_back(triangle);
        end1 = end2;
    }
}

Sun::~Sun()
{
    // Delete the list of triangle points, but let others clean up the surfaces
    for (uint i = 0; i < trianglePoints.size(); i++)
    {
        delete trianglePoints[i];
    }
}

void Sun::transform(QMatrix4x4 transform)
{
    QVector4D w = QVector4D(0, 0, 0, 1);
    sunOrb->center = QVector3D(transform * (QVector4D(sunOrb->center) + w));
    for (uint i = 0; i < trianglePoints.size(); i++)
    {
        *trianglePoints[i] = QVector3D(transform * (QVector4D(*trianglePoints[i]) + w));
    }
}
