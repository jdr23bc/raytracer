#ifndef SUN_H
#define SUN_H

#include "scenes/scene.h"
#include "triangle.h"
#include "sphere.h"
#include "QMatrix4x4"

/**
 * Describes a sun with alternating colors of rays
 * Sun orb size is initially 1, and triangular rays are created on the x-y plane
 */
class Sun: public Scene
{
public:
    Sun();

    /**
     * Create a new sun scene
     * @param sunColor The color of the sun sphere
     * @param ray1Color The color of the rays around the sun
     * @param ray2Color The color of the rays around the sun
     * @param rayAngle The angle (degrees) describing ray size; must evenly
     * divide 360 degrees and be less than 90 degrees
     * @param rayLength Length of the sun's rays
     */
    Sun(QColor sunColor, QColor ray1Color, QColor ray2Color, int rayAngle, float rayLength);

    ~Sun();

    // Apply the given transform to the sun's surfaces
    void transform(QMatrix4x4 transform);

    // The sun orb
    Sphere* sunOrb;

    // The triangles
    std::vector<Triangle *> triangles;

    // The triangle end points
    std::vector<QVector3D *> trianglePoints;

    const float PI = 3.141592653589793;
};

#endif // SUN_H
