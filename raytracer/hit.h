#ifndef HIT_H
#define HIT_H
#include "QVector3D"
class Surface;

/**
 * Hit describes the point of intersection between a ray and a surface
 */
class Hit
{
public:
    Hit();

    /**
     * @param hit True if an surface was intersected, false otherwise
     */
    Hit(bool hit);

    /**
     * @brief Hit
     * @param hitPoint Point on the surface that was hit
     * @param distance The distance from the surface to the ray origin
     * @param surface The surface that was hit
     * @param normal The normal of the surface at the hit point
     */
    Hit(QVector3D hitPoint, float distance, Surface* surface, QVector3D normal);

    // True if there was an intersection, false otherwise
    bool hit;

    // The point on the surface hit by the ray
    QVector3D hitPoint;

    // The surface hit by the ray
    Surface* surface;

    // The normal of the surface at the hitPoint
    QVector3D normal;

    // The distance from the ray origin to the hit point
    float distance;
};

#endif // HIT_H
