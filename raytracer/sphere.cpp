#include "sphere.h"
#include "boundingbox.h"
#include "math.h"

Sphere::Sphere()
{
}

Sphere::Sphere(QVector3D center, float radius, QColor color)
{
    this->center = center;
    this->radius = radius;
    this->diffuseColor = color;
    this->spectralColor = color;
    this->ambientColor = color;
    this->phongExponent = 100;
}

Sphere::Sphere(QVector3D center, float radius, QColor color, float phongExponent)
{
    this->center = center;
    this->radius = radius;
    this->diffuseColor = color;
    this->spectralColor = color;
    this->ambientColor = color;
    this->phongExponent = phongExponent;
}

Sphere::~Sphere()
{
}

Hit Sphere::hit(Ray ray)
{
    float a = sum(ray.direction * ray.direction);
    float b = sum(ray.direction * (2.0f * ( ray.origin - center)));
    float c = sum(center * center) + sum(ray.origin * ray.origin) -2.0f * sum(ray.origin * center) - radius * radius;
    float d = b*b + (-4.0f)*a*c;

    // If the discriminant is less than zero then the ray does not hit the sphere
    if (d < 0)
    {
        return Hit(false);
    }

    // Ray can intersect the sphere, find the closest hitpoint
    float t = (-0.5f)*(b+sqrt(d))/a;

    if (t < 0.0f)
    {
        return Hit(false);
    }

    float dist = (sqrt(a)*t);
    QVector3D hitPoint = dist * ray.direction + ray.origin;

    return Hit(hitPoint, dist, this, getNormal(hitPoint));
}

QVector3D Sphere::getNormal(QVector3D point)
{
    return (point - center).normalized();
}

BoundingBox Sphere::getBoundingBox()
{
    QVector3D rads = QVector3D(radius, radius, radius);
    QVector3D min = center - rads;
    QVector3D max = center + rads;
    return BoundingBox(min, max);
}

float Sphere::sum(QVector3D v)
{
    return v.x() + v.y() + v.z();
}
