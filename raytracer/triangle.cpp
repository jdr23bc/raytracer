#include "triangle.h"
#include "boundingbox.h"
#include "QMatrix4x4"
#include "limits"

Triangle::Triangle()
{
}

Triangle::Triangle(QVector3D * v1, QVector3D * v2, QVector3D * v3, QColor color)
{
    this->v1 = v1;
    this->v2 = v2;
    this->v3 = v3;
    this->diffuseColor = color;
    this->spectralColor = color;
    this->ambientColor = color;
    this->phongExponent = 1;

    QVector3D v12 = *v2 - *v1;
    QVector3D v13 = *v3 - *v1;
    normal = QVector3D::crossProduct(v12, v13).normalized();
}

Triangle::Triangle(QVector3D * v1, QVector3D * v2, QVector3D * v3, QColor color, float phongExponent)
{
    this->phongExponent = phongExponent;
    this->v1 = v1;
    this->v2 = v2;
    this->v3 = v3;
    this->diffuseColor = color;
    this->spectralColor = color;
    this->ambientColor = color;
    this->phongExponent = 1;

    QVector3D v12 = *v2 - *v1;
    QVector3D v13 = *v3 - *v1;
    normal = QVector3D::crossProduct(v12, v13).normalized();
}

Triangle::~Triangle()
{
    // Do not delete vectors because may be used by other triangles
}

// TODO speed this up, the conversions probably are not very efficent
Hit Triangle::hit(Ray ray)
{
    // Vector from ray origin to v1
    QVector4D vO1 = QVector4D(*v1) - QVector4D(ray.origin);

    // Vector from v3 to v1
    QVector4D v31 = *v1 - *v3;

    // Vector from v2 to v1
    QVector4D v21 = *v1 - *v2;

    // Direction vector
    QVector4D d = QVector4D(ray.direction);

    QMatrix4x4 mBeta = QMatrix4x4();
    mBeta.setToIdentity();
    mBeta.setColumn(0, vO1);
    mBeta.setColumn(1, v31);
    mBeta.setColumn(2, d);

    QMatrix4x4 mLambda = QMatrix4x4();
    mLambda.setToIdentity();
    mLambda.setColumn(0, v21);
    mLambda.setColumn(1, vO1);
    mLambda.setColumn(2, d);

    QMatrix4x4 mDist = QMatrix4x4();
    mDist.setToIdentity();
    mDist.setColumn(0, v21);
    mDist.setColumn(1, v31);
    mDist.setColumn(2, vO1);

    QMatrix4x4 mAlpha = QMatrix4x4();
    mAlpha.setToIdentity();
    mAlpha.setColumn(0, v21);
    mAlpha.setColumn(1, v31);
    mAlpha.setColumn(2, d);

    float detAlpha = mAlpha.determinant();

    float lambda = mLambda.determinant()/detAlpha;
    if (lambda < 0 || lambda > 1)
    {
        return Hit(false);
    }

    float beta = mBeta.determinant()/detAlpha;
    if (beta < 0 || beta > 1 - lambda)
    {
        return Hit(false);
    }

    float dist = mDist.determinant()/detAlpha;
    if (dist <= 0)
    {
        return Hit(false);
    }
    QVector3D hitPoint = ray.direction * dist + ray.origin;

    return Hit(hitPoint, dist, this, normal);
}

BoundingBox Triangle::getBoundingBox()
{
    std::vector<QVector3D> vectors;
    vectors.push_back(*v1);
    vectors.push_back(*v2);
    vectors.push_back(*v3);
    return BoundingBox::getBoundingBox(vectors);
}
