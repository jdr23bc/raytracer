#ifndef SURFACE_H
#define SURFACE_H

#include "QVector3D"
#include "QColor"
#include "ray.h"
#include "hit.h"

class BoundingBox;

class Surface
{
public:
    virtual ~Surface(){}

    QColor diffuseColor;
    QColor spectralColor;
    QColor ambientColor;

    // Variable that controls 'shinyness' of the surface
    float phongExponent;

    /**
     * @param ray
     * @return The hit object describing the intersection
     */
    virtual Hit hit(Ray ray)=0;

    /**
     * @brief getBoundingBox
     * @return the minimum bounding box for this surface
     */
    virtual BoundingBox getBoundingBox()=0;
};

#endif // SURFACE_H
