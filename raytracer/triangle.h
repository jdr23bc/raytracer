#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "surface.h"

class Triangle : public Surface
{
public:
    Triangle();
    Triangle(QVector3D * v1, QVector3D * v2, QVector3D * v3, QColor color);
    Triangle(QVector3D * v1, QVector3D * v2, QVector3D * v3, QColor color, float phongExponent);
    ~Triangle();

    QVector3D * v1;
    QVector3D * v2;
    QVector3D * v3;
    QVector3D normal;

    // See description in surface.h
    Hit hit(Ray ray);

    // See description in surface.h
    BoundingBox getBoundingBox();
};

#endif // TRIANGLE_H
