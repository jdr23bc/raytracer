#ifndef RAY_H
#define RAY_H

#include "QVector3D"

class Ray
{
public:
    Ray();

    Ray(QVector3D origin, QVector3D direction);

    ~Ray();

    // The origin of the ray
    QVector3D origin;

    // The direction of the ray
    QVector3D direction;

    // The inverse of the direction of the ray
    QVector3D invDirection;

    // Used to determine what corner of bounding box to use in hit detection
    int signX;
    int signY;
    int signZ;
};

#endif // RAY_H
