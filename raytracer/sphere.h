#ifndef SPHERE_H
#define SPHERE_H

#include "surface.h"

class Sphere: public Surface
{
public:
    Sphere();
    ~Sphere();
    Sphere(QVector3D center, float radius, QColor color);
    Sphere(QVector3D center, float radius, QColor color, float phongExponent);

    // The center of the sphere
    QVector3D center;

    // The radius of the sphere
    float radius;

    // See description in surface.h
    Hit hit(Ray ray);

    // See description in surface.h
    BoundingBox getBoundingBox();

    /**
     * @param point
     * @return Return the normal on the sphere's surface at the given point
     */
    QVector3D getNormal(QVector3D point);

    // Return the sum of the points in the given vector
    float sum(QVector3D v);
};

#endif // SPHERE_H
