#include "ray.h"

Ray::Ray()
{
}

Ray::Ray(QVector3D origin, QVector3D direction)
{
    this->origin = origin;
    this->direction = direction;
    invDirection = QVector3D(1/direction.x(), 1/direction.y(), 1/direction.z());
    signX = (invDirection.x() < 0);
    signY = (invDirection.y() < 0);
    signZ = (invDirection.z() < 0);
}

Ray::~Ray()
{
}
